/**********************************************
* File: nothing.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This is a program that prints my name and email. 
**********************************************/

/* This comment is for FileZilla update*/
#include <stdio.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(void) {

	fprintf(stdout, "My name is Chloe, ccrusan@nd.edu\n");
	return 0;
}

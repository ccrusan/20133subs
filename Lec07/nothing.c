/**********************************************
* File: nothing.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This is a program that does nothing 
**********************************************/

/* This comment is for FileZilla update*/

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(void) {

	return 0;
}

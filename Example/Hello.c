/********************************************
* Function Name  : Hello.c 
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
*
* This file contains a program to run Hello, World
*  
********************************************/
#include <stdio.h> /* standard I/O library*/

/********************************************
* Function Name  :  main
* Pre-conditions : void
* Post-conditions: int

 This is the main driver of program*  
********************************************/

int main(void){
	fprintf(stdout, "Hello, World\n");
	return 0;
}

Chloe Crusan
ccrusan@nd.edu
I pledge my Honor that I have not cheated, nor will I cheat, on this project.

make CA01  

1. Initial unsigned int output in decimal:
	This is the initial value of agressiveness in decimal
2. Initial unsigned int output value in hexadecimal
	This is the initial value of agressiveness in hex
3. Final unsigned int output in decimal
	This is the value -1 but stored as an unsigned int
Hint: In the lecture notes, the result for the aggressiveness was 255. In this example, the result is printed as 4294967295. Explain why the unsigned int prints 4294967295, instead of 255, to the screen for the unsigned int result.
4. Final unsigned int output value in hexadecimal
	This is the value -1 in hex stored as an unsigned int
5. Final signed int output in decimal
	This is the value of agressiveness after subtraction: -1 stored as an int in decimal
6. Final signed int output value in hexadecimal
	This is that same value but in hex

(*NOTE*: I couldn't get it to work for some reason, I changed the type and everything, I don't know why signedAgressiveness = agressiveness)

Results of program:
This program takes an agressiveness score and subtracts a democracy score to get a new agressiveness score. 
First we see the initial value of both agressiveness and democracy, then we subtract the two to get the new score. 
The new score is stored as an unsigned int and a signed int then output as in decimal and hex.
The difference between unsigned and signed is the capability to be a negative number. By storing it in the wrong type, we get unexpected results.

Edit 9/20/2019: The output specifier should have been set to %u 

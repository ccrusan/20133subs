/**********************************************
* File: CA01.txt
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This is the program for the Ghadi Civilization 
**********************************************/

#include <stdio.h> /* fprintf */

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
* This program shows the value of unsigned and signed ints and correctly storing them so as not to blow up Ghandi's civilization

********************************************/

int main(void){


	/* Variables */
	unsigned int agressivness = 1;
	unsigned int democracy = 2;
	unsigned int agressivness1;
	
	/* Output of variables */
	fprintf(stdout, "Before:\n");
	fprintf(stdout, "Decimal value of agressivness is: %ld\n", agressivness);
	fprintf(stdout, "Hex value of agressivness is: %lx\n", agressivness);
	fprintf(stdout, "Decimal value of democracy is: %ld\n", democracy);
	fprintf(stdout, "Hex value of democracy is: %lx\n", democracy);
	
	/* Performing subtraction */
	fprintf(stdout, "\nCalculating agressivness and democracy Difference\n");
	agressivness1 = agressivness - democracy;
	fprintf(stdout, "Decimal value of agressivness is: %u\n", agressivness1);
	fprintf(stdout, "Hex value of agressivness is: %x\n", agressivness1);
	
	int signedNewAgressivness;
	int signedAgressivness = (int)agressivness;
	int signedDemocracy = (int)democracy;
	
	signedNewAgressivness = signedAgressivness - signedDemocracy;

	fprintf(stdout, "Signed agressivness in decimal is: %d\n", signedNewAgressivness); 
	fprintf(stdout, "Signed agressivness in hex is: %x\n", signedNewAgressivness);	

	return 0;
}

/* Circle.cpp */

#include "Circle.h"


double Circle::getRadius() const{
	return radius;
}

double Circle::getDiameter() const{
	return 2*radius;
}

double Circle::getArea() const{
	return 3.14159*radius*radius;
}

void Circle::setRadius(douoble inRad){
	
	radius = inRad;
}

bool operator==(const Circle& rhs) const{
	
	return radius == rhs.radius;
}

std::ostream& operator<<(std::ostream& out, const Circle& C){
	out << C.radius;
	return out;
}
/* Circle.h */
#ifdef CIRCLE_H
#define CIRCLE_H

#include <iostream>

class Circle{

	private:
		double radius;
		
		
	public:
		
		Circle() : radius(1) {}
		
		Circle(double radIn) : radius(radIn) {}
		
		double getRadius() const; 
		
		double getDiameter() const;
		
		double getArea() const;
		
		void setRadius(douoble inRad);
		
		bool operator==(const Circle& rhs) const;
		
		friend std::ostream& out operator<<(std::ostream& out, const Circle& C);























};
#endif

/**********************************************
* File: CirTest.cpp
* Author: 
* Email: @nd.edu
* 
* This is for In-Class Assignment for Lecture 28
* Driver for the Circle Class 
**********************************************/

#include "Circle.h"


int main(){
	
	Circle c1;
	const Circle c2(4);
	
	std::cout << "c1: Radius = " << c1 << ", Area = " << c1.getArea() 
		<< ", Diameter = " << c1.getDiameter << std::endl;

	std::cout << "c2: Radius = " << c2 << ", Area = " << c2.getArea() 
		<< ", Diameter = " << c2.getDiameter << std::endl;
		
	c1.setRadius(10);
	
	std::cout << "c1: Radius = " << c1 << ", Area = " << c1.getArea() 
		<< ", Diameter = " << c1.getDiameter << std::endl;
		
	Circle c3(10);
	
	if(c1 == c4){
		std::cout << "c1 == c4" << std::endl;
	}

	return 0;
}

/**********************************************
* File: doublell.c
* Author: Chloe Crusan, Alisa Nguyen, Bridget Goodwine, Sophie Canepa
* Email: ccrusan@nd.edu, anguye22@nd.edu, bgoodwin@nd.edu, scanepa@nd.edu
* 
* This file contains the function definitions and 
* structs for a Sorted Linked List Data structure 
**********************************************/

#include "doublell.h"

/********************************************
* Function Name  : CreateNode
* Pre-conditions : void
* Post-conditions: NODEPTR
* 
* CreateNode initializes the members
* CreateNode exits if there is insufficient
* memory
********************************************/
NODEPTR CreateNode (void)
{
   NODEPTR newNode;

   /* Get the space needed for the node */
   newNode = (NODEPTR) malloc (sizeof(NODE));
   if (newNode == NULL)
   {
      fprintf (stderr, "Out of Memory - CreateNode\n");
      exit (-1);
   }

   /* Initialize the members */
   newNode -> data = 0;
   newNode -> next = NULL;

   return newNode;
}

/********************************************
* Function Name  : SetData
* Pre-conditions : NODEPTR temp, int data
* Post-conditions: void   
*  
* the data member of the node pointed to 
* is populated with the value passed in
********************************************/
void SetData (NODEPTR temp, int value)
{
   temp -> data = value;
} 



/******************
* Insert takes a pointer to a NODEPTR, the
* address of head, and a NODEPTR, temp as 
* arguments.  temp is a pointer to the
* node to be inserted.  The node is then
* inserted into the list at the appropriate
* place to maintain the list as a sorted list.
******************/ 
void Insert (NODEPTR* headPtr, NODEPTR temp)
{
   NODEPTR prev, curr;

   if ( IsEmpty (*headPtr))
   {
       *headPtr = temp;
   }
   else
   {
      prev = NULL;
      curr = *headPtr;

      /* traverse the list until the spot
      for insertion is found */
      while (curr != NULL && temp->data > curr-> data)
      {
		 prev = curr;
		 curr = curr -> next;
      }
  
      /* insert the node, temp */
      if(prev == NULL)
      {
		 temp -> next = *headPtr;
		 *headPtr = temp;
      }
      else
      {
		 prev -> next = temp;
		 temp -> next = curr;
      }
   }
}


/********************************************
* Function Name  : Delete
* Pre-conditions : NODEPTR* headPtr, int data
* Post-conditions: int    
*  
* Delets the first instance of data. Returns -1 
* if not found
********************************************/
int Delete (NODEPTR* headPtr, int target)
{
   int value;
   NODEPTR temp, prev, curr;

   if (IsEmpty (*headPtr))
   {
      printf ("Can't delete from an empty list\n");
      return (-1);
   }

   /* if the target value is the first
   in the list, move head */
   else if (target == (*headPtr) -> data)
   {
      temp = *headPtr;
      value = (*headPtr) -> data;
	  if( (*headPtr) -> next == NULL 0){
		*headPtr = NULL;
		free (temp);
		return (value); 
	  }
	else{
      *headPtr = (*headPtr) -> next;
	  (*headPtr)-> prev = NULL;
      free (temp);
      return (value);
	}
   }

   else
   {
      prev = *headPtr;
      curr = (*headPtr) -> next;

      while (curr != NULL &&
	     curr -> data != target)
      {
		 prev = curr;
		 curr = curr -> next;
      }
      
      if(curr != NULL)
      {
		  if(curr -> next == NULL){
	
		/* delete the node the contains
		the target value */
		temp = curr;
		prev -> next = NULL;
		//curr -> next -> prev = prev;
		value = curr -> data;
		free(temp);
		return(value);
		
		}
		
		else{
		/* delete the node the contains
		the target value */
		temp = curr;
		prev -> next = curr -> next;
		curr -> next -> prev = prev;
		value = curr -> data;
		free(temp);
		return (value);
		
		}
	  }
      else
      {
		fprintf(stdout, "%d was not in the list\n", target);
		return (-1);
      }
   }      
}


/********************************************
* Function Name  : IsEmpty
* Pre-conditions : NODEPTR head
* Post-conditions: int    
* 
* returns 1 (true) if the list is empty
* returns 0 (false) if the list is not empty
********************************************/
int IsEmpty (NODEPTR head)
{
   /* If the pointer to the list is
   NULL then there is no list.  The
   list is empty, so we return true.
   */
   return (head == NULL);
}


/********************************************
* Function Name  : PrintList
* Pre-conditions : NODEPTR head
* Post-conditions: void    
* 
* each node in the list is printed according to the format specified in this code
********************************************/
void PrintList (NODEPTR head)
{
   NODEPTR curr;

   if (IsEmpty (head))
   {
      fprintf (stdout, "The list is empty\n");
   }
   else
   {
      /* set the current pointer to the first
      ** node of the list */
      curr = head;

      /* Until the end of the list */
      while (curr != NULL)
      {
          /* print the current data item */
          fprintf (stdout, "%d ", curr -> data);

	  /* move to the next node */
	  curr = curr -> next;
      }
      fprintf (stdout, "\n");
   }   
}

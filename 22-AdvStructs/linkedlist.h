/**********************************************
* File: linkedlist.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function definitions and 
* structs for a Linked List Data structure 
**********************************************/

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdio.h>
#include <stdlib.h>


/* Node pointer typedef */
typedef struct node * NODEPTR;


/* NODE contains the data and a pointer to the next node */
typedef struct node
{
    int     data;
    NODEPTR next;    /* OR struct node *next; */
}NODE;


/********************************************
* Function Name  : CreateNode
* Pre-conditions : void
* Post-conditions: NODEPTR
* 
* CreateNode initializes the members
* CreateNode exits if there is insufficient
* memory
********************************************/
NODEPTR CreateNode (void);


/********************************************
* Function Name  : SetData
* Pre-conditions : NODEPTR temp, int data
* Post-conditions: void   
*  
* the data member of the node pointed to 
* is populated with the value passed in
********************************************/
void    SetData    (NODEPTR temp, int data);


/********************************************
* Function Name  : Insert
* Pre-conditions : NODEPTR* headPtr, NODEPTR temp
* Post-conditions: void   
* 
* the node is inserted at the end of the linked list
********************************************/
void    Insert     (NODEPTR* headPtr, NODEPTR temp);


/********************************************
* Function Name  : Delete
* Pre-conditions : NODEPTR* headPtr, int data
* Post-conditions: int    
*  
* Delets the first instance of data. Returns -1 
* if not found
********************************************/
int     Delete     (NODEPTR* headPtr, int data);


/********************************************
* Function Name  : IsEmpty
* Pre-conditions : NODEPTR head
* Post-conditions: int    
* 
* returns 1 (true) if the list is empty
* returns 0 (false) if the list is not empty
********************************************/
int     IsEmpty    (NODEPTR head);


/********************************************
* Function Name  : PrintList
* Pre-conditions : NODEPTR head
* Post-conditions: void    
* 
* each node in the list is printed according to the format specified in this code
********************************************/
void    PrintList  (NODEPTR head);

#endif

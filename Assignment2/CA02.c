/**********************************************
* File: CA02.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This is assignment 2 
* This assignment uses knowledge of pointer arithmetic to print out the distances to walk from Stinson-Remick Hall 
**********************************************/
#include<stdio.h> //for fprintf
#include<stdlib.h> //for malloc
/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver for the program 
********************************************/
int main(void){

	/* Declare variables */
	long unsigned int arrayLen = 5; 
	int *distArr = malloc(arrayLen * sizeof(int) ); 	//array pointer of distances in feet 
	
	/* Allocate values */
	distArr[0] = 0x515;				//SR to ND stadium, 1301 in hex is 515
	*((int*)distArr+1) = 765;		//SR to Fitz, pointer arithmetic notation
	((int *)distArr)[2] = 2548;		//SR to Hesberg
	*((int*)distArr+3) = 2343;		//SR to Main Building, pointer arithmetic notation
	distArr[4] = 2133;				//SR to Basilica
	
	/* Print out distances */
	fprintf(stdout, "Stinson-Remick to ND Stadium Student Gate: %d\n", ((int *)distArr)[0]);
	fprintf(stdout, "Stinson-Remick to Fitzpatrick Hall: %d\n", *((int *)distArr + 1));			//"value pointed to by" pointer arithmetic notation
	fprintf(stdout, "Stinson-Remick to Hesberg Library: %d\n", *((int *)distArr + 2));			//"value pointed to by" pointer arithmetic notation
	fprintf(stdout, "Stinson-Remick to Main Building: %d\n", ((int *)distArr)[3]);
	fprintf(stdout, "Stinson-Remick to Basilica: %d\n", ((int *)distArr)[4]);
	
	/* Free Memory */
	free(distArr);
	



	return 0;

}

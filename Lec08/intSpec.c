/**********************************************
* File: intSpec.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This is to show integer output specifies  
**********************************************/

#include <stdio.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(void){
	
	int example1 = 10;
	int example2 = 20;
	unsigned int unEx1 = 10;
	unsigned int unEx2 = 20;
	
	int example3 = example1 - example2;
	
	fprintf(stdout, "example3 = %d\n", example3);
	fprintf(stdout, "example3 = %x\n", example3);
	
	unsigned int unEx3 = unEx1-unEx2;
	
	fprintf(stdout, "unEx3 = %u\n", unEx3);
	fprintf(stdout, "unEx3 = %x\n", unEx3);
	
	return 0;
}



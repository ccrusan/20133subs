/**********************************************
* File: ArrayEx.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This is an in-class example of pointer arithmetic and arrays, and malloc 
**********************************************/

#include <stdio.h>
#include <stdlib.h> /* For Malloc */

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Declare variables */
	long unsigned int arraySize = 5;
	void *voidArr = malloc(arraySize * sizeof(char) );
	
	/* Allocate values */
	*((char *)voidArr + 0) = 78;
	((char *)voidArr)[1] = 0157;
	*((char *)voidArr +2) = 0x74;
	((char *)voidArr)[3] = 100 + 14;
	((char *)voidArr)[4] = 'e';
	
	fprintf(stdout, "%c", ((char *)voidArr)[0]);
	fprintf(stdout, "%c", *((char *)voidArr + 1));
	fprintf(stdout, "%c", *((char *)voidArr + 2));	
	fprintf(stdout, "%c", ((char *)voidArr)[3]);
	fprintf(stdout, "%c\n", *((char *)voidArr + 4));	
	
	long unsigned int dameSize = 4;
	char* dameArray = malloc(dameSize * sizeof(char));
	
	/* dameArray = "Dame"; Do not re-allocate with quotations*/
	dameArray[0] = 'D';
	*(dameArray + 1) = 'a'; //pointer arithmetic
	dameArray[2] = 'm'; //abstsracted pointer arithmetic
	*(dameArray + 3) = 'e';	 
	
	fprintf(stdout, "%s\n", dameArray);
	
	/* Free Memory */
	free(voidArr);
	free(dameArray);
	
	return 0;

}

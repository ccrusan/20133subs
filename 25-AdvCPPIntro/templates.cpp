/**********************************************
* File: templates.cpp
* Author:Chloe Crusan 
* Email: ccrusan@nd.edu
* 
* This file contains a driver for learning 
* function overloading and templates in C++ 
**********************************************/

#include <iostream>
#include <string>
#include <vector>

void printComp(std::vector< int > intVec, int comp){
	
	for(unsigned int iter = 0; iter < intVec.size(); ++iter){
		
		if(intVec.at(iter) < comp){
			
			std::cout << intVec.at(iter) << " is less than " << comp << std::endl;
			
		}
		
	}
	
	std::cout << std::endl;
	
}

void printComp(std::vector< float > floatVec, float comp){
	
	for(unsigned int iter = 0; iter < floatVec.size(); ++iter){
		
		if(floatVec.at(iter) != comp){
			
			std::cout << floatVec.at(iter) << " does not equal " << comp << std::endl;
			
		}
		
	}
	
	std::cout << std::endl;
	
}

template<class T>
void printVec(std::vector< T > theVec){
	
	for(unsigned int iter = 0; iter < theVec.size(); ++iter){
		
		std::cout << theVec.at(iter) << std::endl;
		
	}
	
	std::cout << std::endl;
	
}

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	std::vector< int > intVec;
	std::vector< float > floatVec;
	std::vector< std::string > stringVec(4, "Notre Dame");
	
	intVec.push_back(10);
	intVec.push_back(20);
	intVec.push_back(30);
	printComp(intVec, 25);
	
	floatVec.push_back( (float)25.1);
	floatVec.push_back( (float)-8.7);
	floatVec.push_back( (float)0);
	floatVec.push_back( (float)1.1e-7);
	printComp(floatVec, 0);
	
	stringVec.at(0) = "Cheer, cheer for old ";
	stringVec.at(2) = ". Wake up the echos ";
	stringVec.at(3) = "cheering her name. ";
	
	printVec(intVec);
	printVec(floatVec);
	printVec(stringVec);

	return 0;
}

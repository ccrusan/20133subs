/**********************************************
* File: ptrArr.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
*In-class assignment for POinter and Array Intro
**********************************************/

#include <stdio.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Declare variables*/
	char charEx = 0x4e;
	unsigned char unChEx = 'D';
	char intCh = 33;
	unsigned char chOver = 140;
	
	fprintf(stdout, "Char Example:\n");
	
	fprintf(stdout,"charEx char: %c\n", charEx);
	fprintf(stdout, "charEx decimal: %d\n", charEx);
	fprintf(stdout, "charEx hex: %x\n\n", charEx);
	
	fprintf(stdout,"unChEx char: %c\n", unChEx);
	fprintf(stdout, "unChEx decimal: %d\n", unChEx);
	fprintf(stdout, "unChEx hex: %x\n\n", unChEx);
	
	fprintf(stdout,"intCh char: %c\n", intCh);
	fprintf(stdout, "intCh decimal: %d\n", intCh);
	fprintf(stdout, "intCh hex: %x\n\n", intCh);
	
	fprintf(stdout,"chOver char: %c\n", chOver);
	fprintf(stdout, "chOver decimal: %d\n", chOver);
	fprintf(stdout, "chOver hex: %x\n\n", chOver);

	/* First Pointer Example */
	int intEx = 1;
	int *firstPtr;
	
	fprintf(stdout, "Pointer example 1:%c", 10);
	fprintf(stdout, "firstPtr register address: %p\n", (void *)&firstPtr);	//%p is output specifier for pointer
	fprintf(stdout, "firstPtr value is %p\n", (void*)firstPtr);
	fprintf(stdout, "&intEx is %p\n", (void*)&intEx);
	fprintf(stdout, "value pointed to by firstPtr: %d\n\n", *firstPtr);
	
	int swapX = 10, swapY = 20;
	int *ptrX, *ptrY;
		
	/* Allocate pointers */
	ptrX = &swapX;
	ptrY = &swapY;
	
	fprintf(stdout, "Initial values: %d %d\n", swapX, swapY);
	fprintf(stdout, "Reg addrs: %p %p\n", (void*)&ptrX, (void*)&ptrY);
	fprintf(stdout, "Reg values: %p %p\n", (void*)ptrX, (void*)ptrY);
	fprintf(stdout, "Int addrs: %p %p\n", (void*)&swapX, (void*)&swapY);
	fprintf(stdout, "Values pointed to by: %d %d\n\n", *ptrX, *ptrY);
	
	ptrX = ptrY;
	fprintf(stdout, "After ptrX = ptrY: %d %d\n", swapX, swapY);
	fprintf(stdout, "Reg addrs: %p %p\n", (void*)&ptrX, (void*)&ptrY);
	fprintf(stdout, "Reg values: %p %p\n", (void*)ptrX, (void*)ptrY);
	fprintf(stdout, "Int addrs: %p %p\n", (void*)&swapX, (void*)&swapY);
	fprintf(stdout, "Values pointed to by: %d %d\n\n", *ptrX, *ptrY);
	
	ptrY = &swapX;
	
	fprintf(stdout, "After ptrY = &swapX: %d %d\n", swapX, swapY);
	fprintf(stdout, "Reg addrs: %p %p\n", (void*)&ptrX, (void*)&ptrY);
	fprintf(stdout, "Reg values: %p %p\n", (void*)ptrX, (void*)ptrY);
	fprintf(stdout, "Int addrs: %p %p\n", (void*)&swapX, (void*)&swapY);
	fprintf(stdout, "Values pointed to by: %d %d\n\n", *ptrX, *ptrY);
	
	
	
	
	
	
	return 0;
}

/**********************************************
* File: sin.cpp
* Author: 
* Email: @nd.edu
* 
* This function contains the main driver function
* for doing sin(x) using recursion 
**********************************************/
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <cstdlib>		// exit
#include <cmath>        // pow

double getArgv1Num(int argc, char** argv){
	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}

	//stringstream used for the conversion intitialized with the contents of argv[1]
	double factNum;
	std::stringstream convert(argv[1]);
	
	//give the value to factNum using the characters in the string
	if ( !(convert >> factNum) ){
		std::cout << "Not a valid double" << std::endl;
		exit(-1);
	}
	
	return factNum;
}

double factorial(unsigned int i){
	
	//base case i = 0
	if(i == 0){
		return 1;
	}
	
	return i* factorial(i-1);
}

double sin(double x, int n){
	
	if(n == 0){
		return x;
	}
	
	return pow(-1, n)*pow(x, 2*n+1)/factorial(2*n+1) + sin(x, n-1);

}
		
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(int argc, char** argv){
	
	std::cout << sin(getArgv1Num(argc,argv), 20) << std::endl;


	return 0;
}

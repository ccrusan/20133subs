/**********************************************
* File: Repitition.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This file contains examples of repitition statment combining concepts with pointers and Finite State Machines 
**********************************************/

#include <stdio.h>
#include<stdlib.h>
#include<stdbool.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Use for loop to traverse pointer arrays*/
	long unsigned int ArrSize = 5;
	void* voidArr = malloc(ArrSize * sizeof(char));
	
	*( (char *)voidArr + 0) = 'N';
	( (char *)voidArr)[1] = 'o';
	*( (char *)voidArr + 2) = 0x74;
	( (char *)voidArr)[3] = 114;		//can put in decimal value as long as it is less than 127
	*( (char *)voidArr + 4) = 0145;
	
	fprintf(stdout, "Base Address of voidArr register is %p\n", (void *)&voidArr);
	
	fprintf(stdout, "Address pointed to by voidArr in Data Memory is %p\n", voidArr);
	
	long unsigned int arrLoc;
	for(arrLoc = 0; arrLoc < ArrSize; ++arrLoc){
		
		fprintf(stdout, "Address of voidArr[%lu] is %p\n", arrLoc, ( ((char *) voidArr) + arrLoc));
		
		fprintf(stdout, "Value pointed to by voidArr[%lu] is %c\n", arrLoc, *( ((char *) voidArr) + arrLoc));
	}
	
	free(voidArr);
	
	/* Now we're going to do the same thing for an integer array */
	int* intArr = malloc(ArrSize * sizeof(int));
	
	
	/* Allocate elements individually using a for loop */
	for(arrLoc = 0; arrLoc < ArrSize; ++arrLoc){
		
		intArr[arrLoc] = (int)arrLoc * 2;
		
	}
	
	fprintf(stdout, "Base address of intArr register is %p\n", (void *)&intArr);
 
	fprintf(stdout, "Address pointed to by intArr register in Data Memory is %p\n", (void *)intArr);
	
	for(arrLoc = 0; arrLoc < ArrSize; ++arrLoc){
		
		fprintf(stdout, "Address of intArr[%lu] is %p and ", arrLoc, (void *)&intArr[arrLoc]);
		
		fprintf(stdout, "value pointed to by intArr[%lu] is %d\n", arrLoc, intArr[arrLoc]);
	}
	
	
	/* Free pointers */
	free(intArr);
	
	
	




	return 0;

}

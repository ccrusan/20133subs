/**********************************************
* File: advTypes.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
*
* Class example for floats and doubles
* Emphasizes how IEEE 754 is implemented in the computing device  
**********************************************/
#include <stdio.h>
#include <math.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
*  
********************************************/
int main(void){
	
	/* Variable Declarations */
	float f_value = (float)-1.0125 * (float)pow(2, -127);
	double d_value = 1.8725 * pow(2, 205);
	float x_comp = (float)1.2;
	float y_comp = (float)1.1;
	
	fprintf(stdout, "Decimal f_value: %.45f\n", f_value);
	fprintf(stdout, "Scientific f_value: %e\n", f_value);
	fprintf(stdout, "Hexadecimal f_value: %a\n", f_value);
	
	fprintf(stdout, "Decimal d_value: %.2lf\n", d_value);
	fprintf(stdout, "Scientific d_value: %e\n", d_value);
	fprintf(stdout, "Hexadecimal d_value: %a\n", d_value);
	
	float f_result = x_comp - y_comp;
	fprintf(stdout, "Decimal f_result: %.24f\n", f_result);
	fprintf(stdout, "Scientific f_result: %.23e\n", f_result);
	fprintf(stdout, "Hexadecimal f_result: %a\n", f_result);	
	
	return 0;

}

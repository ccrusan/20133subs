/**********************************************
* File: Selection.c
* Author: Chloe Crusan
* Email: ccrusan@nd.edu
* 
* This program shows examples of selection statements 
**********************************************/

#include <stdio.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Declare variables */
	// int selectNum = 0;
	// int selectNum = 1;
	// int selectNum = 2;
	// int selectNum = 3;
	int selectNum = 4;
	
	/* If-Else Statement */
	int x = 1, y = 2;
	
	/* While Loop Comparison */
	int sum = 0;
	int whileNum = 10;
	
	/* For loop - Fibonacci */
	int forNum = 13;
	int f1 = 1, f2 = 1, fib;
	
	/* Float if comparison */
	float fCompX = (float)1.2, fCompY = (float)1.1;
	
	/* Select Statement */
	switch(selectNum){
		
		case 0:
			if(x == y){
				fprintf(stdout, "x==y\n");
			}
			else if(x + y == 3){
				fprintf(stdout, "x+y == 3\n");
			}
			else{
				fprintf(stdout, "x != y and their sum is not 3\n");
			}
			
			break;
		case 1:
			fprintf(stdout, "The summation of 1 to %d is ", whileNum);
			
			while( whileNum>0 ){
				sum += whileNum;
				whileNum--;
			}
			
			fprintf(stdout, "%d\n", sum);
			
			break;
			
		case 2:
		
			fprintf(stdout, "The first %d Fib numbers are: %d %d ", forNum + 2, f1, f2);
			
			for(int i = 0; i < forNum; ++i){
				fib = f1 + f2;
				fprintf(stdout, "%d ", fib);
				f1 = f2;
				f2 = fib;
			}
			
			break;
		
		case 3:
			
			if(fCompX - fCompY == 0.1){		//using floats inside of an if statement, not a good idea because floats are an estimate
				fprintf(stdout, "%f - %f = 0.1", fCompX, fCompY);
			}	
			else{
				fprintf(stdout, "%f - %f = %.20f\n", fCompX, fCompY, fCompX - fCompY);
			}
			
			break;
		
		default:
		
			fprintf(stdout, "%d is none of the choices\n", selectNum);
			
			break;
		
	}
	
	
	
	

	return 0;

}
